$(document).ready(function() {
    $('.btn_login_in').on('click', function() {
        $('.login_in').fadeIn();
    });
    $('.login_in_close').on('click', function() {
        $('.login_in').fadeOut();
    });

    $('#add_comment').on('click', function() {
        var comment = $('#comment-add-form form').serialize();
        comment = comment + '&id=' + window.location.search.split('?id=')[1];
        // console.log(comment);
        $.ajax({
            url: "comments/insert.php",
            context: document.getElementById(parent_block_comment),
            data: comment,
            type: 'POST'
        }).done(function(data) {
            $(data).insertAfter($("#parent_block_comment > .article:last-child"));
        });
    });
});