<?php
include('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Блог IT_Минималиста!</title>

  <!-- Bootstrap Grid -->
  <link rel="stylesheet" type="text/css" href="media/assets/bootstrap-grid-only/css/grid12.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

  <!-- Custom -->
  <link rel="stylesheet" type="text/css" href="media/css/style.css">
</head>
<body>

  <div id="wrapper">
  <?php 
include('header.php');

?>
<?php
  $articles = mysqli_query($dbconnect, "select * from articles where id_article=". $_GET['id']);
  $article = mysqli_fetch_assoc($articles);
mysqli_query($dbconnect, "update articles set view = view + 1 where id_article = ".$article['id_article']);

?>
    <div id="content">
      <div class="container">
        <div class="row">
          <section class="content__left col-md-8">
        
                 <div class="block">
              <h3><?=$article['title_article']?></h3><br>
              <div class="views" style='font-size:12px'>Количество просмотров: <?=$article['view']?></div>
              <div class="block__content">
                <img src="<?=$article['img_article']?>">

                <div class="full-text">
                <?=(nl2br($article['description_article']))?>
              </div>
            </div>
          

             <div class="block">
              <h3>Комментарии</h3>
              <div class="block__content">
                <div class="articles articles__vertical">

                  <?php
                
                  $comments = mysqli_query($dbconnect, "select * from comments where id_article = ".$_GET['id']." order by id_comment DESC LIMIT 5 ");
                  if(mysqli_num_rows($comments)<=0)
                  {
                    echo "Комментариев нет";
                  }else
                  ?>
                  <div id="parent_block_comment">
                  <?
                  while($comment = mysqli_fetch_assoc($comments))
                  {
                    ?>
                    <article class="article" style="position:relative;">
                        <div class="article__image" style="background-image: url(/media/images/post-image.jpg);"></div>
                          <div class="article__info">
                            <a href="article.php?id=<?=$comment['id_comment']?>"><?=$comment['nickname_author']?></a>
                            <div class="article__info__meta">
                    
                            </div>
                            <div class="article__info__preview"><?=substr($comment['text_comment'],0,100)?></div>
                            <div class="article__info__preview"><a href="delete_comment.php?id=<?=$comment['id_comment']?>" style="color:black; text-decoration:none;position:absolute;top:0; right:0; width:20px;"><img src="images/1560238353.svg" alt=""></a></div>
                        </div>
                    </article>
                    <?php
                  }
                  ?>
               </div>
                </div>
 
              </div>
            </div>

            <div class="block" id="comment-add-form">
              <h3>Добавить комментарий</h3>
              <div class="block__content">
                <form class="form" method='POST' onsubmit="return false;">
                  
                  <div class="form__group">
                    <div class="row">
                    
                      <div class="col-md-6">
                        <input type="text" class="form__control" required="" name="nickname" placeholder="Никнейм">
                      </div>
                    </div>
                  </div>
                  <div class="form__group">
                    <textarea name="text" required="" class="form__control" placeholder="Текст комментария ..."></textarea>
                  </div>
                  <div class="form__group">
                    <input type="submit" class="form__control" name="do_post" id="add_comment" value="Добавить комментарий" onclick="return false;">
                  </div>
                </form>
              </div>
            </div>
          </section>
          <?php 
          include('sidebar.php');
          
          ?>
        </div>
      </div>
    </div>

    <?php
include('footer.php')

?>

  </div>

</body>
</html>