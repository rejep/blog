<?php
include('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Блог IT_Минималиста!</title>
  <!-- Bootstrap Grid -->
  <link rel="stylesheet" type="text/css" href="/media/assets/bootstrap-grid-only/css/grid12.css">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
  <!-- Custom -->
  <link rel="stylesheet" type="text/css" href="/media/css/style.css">
</head>
<body>
  <div id="wrapper id">
<?php
include('header.php');
?>
   <div id="content">
      <div class="container">
      <div class="login_form">
      <?php
if (isset($_POST['registration_btn_form'])) {
    $login    = $_POST['login'];
    $password = $_POST['password'];
    if (!empty($login) && !empty($password)) {
        $query = mysqli_query($dbconnect, "select * from user where login = '$login'");
        if (mysqli_num_rows($query) == 0) {
            $query = mysqli_query($dbconnect, "insert into user (`login`, `password`) VALUES ('" . $login . "','" . $password . "')");
            
            echo '<span style="color:green"> Регистрация прошла успешно </span>';
            $_POST['login']    = '';
            $_POST['password'] = '';
            
        } else {
            echo '<span style="color:red"> Такой логин уже существует </span>'; 
        }
    }
} elseif (isset($_POST['login_in_btn_form'])) {
    $login    = $_POST['login'];
    $password = $_POST['password'];
    if (!empty($login) && !empty($password)) {
        $query = mysqli_query($dbconnect, "select * from user where login = '$login' and password = '$password'");
        $user  = mysqli_fetch_assoc($query);
        if ($login = $user['login'] && $password = $user['password']) {
            $_SESSION['auth']= $user['login'];
            $_POST['login']    = '';
            $_POST['password'] = '';
           
        } else {
            echo "Логин или пароль введен неправильно";
        }
    }
}
?>


<?php
error_reporting(0);
if ($_SESSION['auth']==''){
  ?>
  <h4>Авторизоваться</h4>
  <div class="login_in_form">
  <form method="POST" class="form form_login" >
      <div class="form__group">
         <input type="text" class="form__control" required="" name="login" placeholder="Login" value="<?= $_POST['login'] ?>" >
      </div>
      <div class="form__group">
        <input type="password" class="form__control" required="" name="password" placeholder="Password" value="<?= $_POST['password'] ?>">
      </div>
      <div class="form__group">
        <input type="submit" class="form__control" name="registration_btn_form" value="Зарегистрироваться">
      </div>
      <div class="form__group">
        <input type="submit" class="form__control" name="login_in_btn_form" value="Войти">
      </div>

  </form>
</div>
<?php
}else{
  echo"Hello " .$_SESSION['auth'];
  echo "<a href='user/logout.php' class='btn_logout'>Выйти</a>";

}
?>

</div>
        <div class="row">
          <section class="content__left col-md-8">
            <div class="block">
              <a href="articles.php" >Все записи</a>
              <h3>Новейшее_в_блоге</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">
                <?php
$articles = mysqli_query($dbconnect, "select * from articles order by id_article DESC LIMIT 4");
?>
               <?php
while ($article = mysqli_fetch_assoc($articles)) {
echo $article['title_article'];
  echo mb_convert_encoding($article['title_article'], 'utf-8', mb_detect_encoding($article['title_article']));


?>
                   <article class="article">
                    <div class="article__image" style="background-image: url(<?= $article['img_article'] ?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?= $article['id_article'] ?>"><?= $article['title_article'] ?></a>
                      <div class="article__info__meta">
                        <?php
    $article_category;
    foreach ($categories as $category) {
      if ($category['id_category'] ==$article['id_category']) {
         
        $article_category = $category;
        
        break;
    }
    }
?>
                       <small>Категория: <a href="article.php?id=<?= $category['id_category'] ?>"><?= $article_category['name_category'] ?></a></small>
                      </div>
                      <div class="article__info__preview"><?= mb_substr($article['description_article'], 0, 100, "UTF-8") . '...' ?></div>
                    
                    </div>
                  </article>
                    <?php
}
?>
               </div>
              </div>
            </div>
        <div class="block">
              <a href="articles.php?category=2">Все записи</a>
              <h3>Искусство [Новейшее]</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">
                <?php
$articles = mysqli_query($dbconnect, "select * from articles where id_category = 2 order by id_article DESC LIMIT 4");
?>
               <?php
while ($article = mysqli_fetch_assoc($articles)) {
?>
                   <article class="article">
                    <div class="article__image" style="background-image: url(<?= $article['img_article'] ?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?= $article['id_article'] ?>"><?= $article['title_article'] ?></a>
                      <div class="article__info__meta">
                        <?php
    $article_category;
    foreach ($categories as $category) {
      if ($category['id_category'] ==$article['id_category']) {
         
        $article_category = $category;
        
        break;
    }
    }
?>
                       <small>Категория: <a href="article.php?id=<?= $category['id_category'] ?>"><?= $article_category['name_category'] ?></a></small>
                      </div>
                      <div class="article__info__preview"><?= mb_substr($article['description_article'], 0, 100, 'utf-8') . '...' ?></div>
                    </div>
                  </article>
                    <?php
}
?>
               </div>
              </div>
            </div>
            <div class="block">
              <a href="articles.php?category=1">Все записи</a>
              <h3>Программирование [Новейшее]</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">
                <?php
$articles = mysqli_query($dbconnect, "select * from articles where id_category = 1 order by id_article DESC LIMIT 4");
?>
               <?php
while ($article = mysqli_fetch_assoc($articles)) {
?>
                   <article class="article">
                    <div class="article__image" style="background-image: url(<?= $article['img_article'] ?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?= $article['id_article'] ?>"><?= $article['title_article'] ?></a>
                      
                      <div class="article__info__meta">
                        <?php
    $article_category;
    foreach ($categories as $category) {
        if ($category['id_category'] ==$article['id_category']) {
         
            $article_category = $category;
            
            break;
        }
    }
?>
                       <small>Категория: <a href="article.php?id=<?= $category['id_category'] ?>"><?= $article_category['name_category'] ?></a></small>
                      </div>
                      <div class="article__info__preview"><?= substr($article['description_article'], 0, 100) . '...' ?></div>
                    </div>
                  </article>
                    <?php
}
?>
               </div>
              </div>
            </div>
          </section>
          <?php
include('sidebar.php');
?>
       </div>
      </div>
    </div>
    </div>  
<?php
include('footer.php');
?>
 </div>
 
</body>
</html>
<!-- david - i don't care -->
<!-- aidan - hurt you (ft. Thomas Reid) -->
<!-- ellskel - anxiety (ft. dhan) -->
<!-- i hate to think about you with somebody else. -->
<!-- Lo'Fi Boy - Sex For Breakfast (ft. Shiloh) -->