<header id="header">
      <div class="header__top">
        <div class="container">
          <div class="header__top__logo">
            <h1>Блог IT_Минималиста</h1>
          </div>
          <nav class="header__top__menu">
            <ul>
              <li><a href="index.php">Главная</a></li>
              <li><a href="#">Обо мне</a></li>
              <li><a href="#">Я Вконтакте</a></li>
            </ul>
          </nav>
         
        </div>
        
      </div>

      <div class="header__bottom">
        <div class="container">
          <nav>
            <?php
            $categories = mysqli_query($dbconnect, "select * from category")
            
            ?>
            <ul>
              <?php
                while($category=mysqli_fetch_assoc($categories))
                {
                  ?>
                       <li><a href="articles.php?category=<?=$category['id_category']?>"><?=$category['name_category']?></a></li>
                  <?php
                }

              ?>
         
            </ul>
          </nav>
        </div>
      </div>
    
    </header>