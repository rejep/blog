<?php
include('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Блог IT_Минималиста!</title>
  <!-- Bootstrap Grid -->
  <link rel="stylesheet" type="text/css" href="/media/assets/bootstrap-grid-only/css/grid12.css">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
  <!-- Custom -->
  <link rel="stylesheet" type="text/css" href="/media/css/style.css">
</head>
<body>
  <div id="wrapper">
<?php
include('header.php');
?>
   <div id="content">
      <div class="container">
        <div class="row">
          <section class="content__left col-md-8">
            <div class="block">
              <a href="articles.php">Все записи</a>
              <h3>Новейшее_в_блоге</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">
                <?php
$articles = mysqli_query($dbconnect, "select * from articles order by id_article DESC LIMIT 4");
?>
               <?php
while ($article = mysqli_fetch_assoc($articles)) {
?>
                   <article class="article">
                    <div class="article__image" style="background-image: url(<?= $article['img_article'] ?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?= $article['id_article'] ?>"><?= $article['title_article'] ?></a>
                      <div class="article__info__meta">
                        <?php
    $article_category;
    foreach ($categories as $category) {
      if ($category['id_category'] ==$article['id_category']) {
         
        $article_category = $category;
        
        break;
            
        }
    }
?>
                       <small>Категория: <a href="article.php?id=<?= $category['id_category'] ?>"><?= $article_category['name_category'] ?></a></small>
                      </div>
                      <div class="article__info__preview"><?= mb_substr($article['description_article'], 0, 100, "UTF-8") . '...' ?></div>
                      <div class="delete">
                        <a href="delete.php?id=<?=$article['id_article'] ?>" class="form__control" style="text-decoration:none; color:red;">Удалить статью</a>
                      </div>
                    </div>
                  </article>
                    <?php
}
?>
               </div>
              </div>
            </div>
        <div class="block">
              <a href="articles.php?category=2">Все записи</a>
              <h3>Искусство [Новейшее]</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">
                <?php
$articles = mysqli_query($dbconnect, "select * from articles where id_category = 2 order by id_article DESC LIMIT 4");
?>
               <?php
while ($article = mysqli_fetch_assoc($articles)) {
?>
                   <article class="article">
                    <div class="article__image" style="background-image: url(<?= $article['img_article'] ?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?= $article['id_article'] ?>"><?= $article['title_article'] ?></a>
                      <div class="article__info__meta">
                        <?php
    $article_category;
    foreach ($categories as $category) {
      if ($category['id_category'] ==$article['id_category']) {
         
        $article_category = $category;
        
        break;
        }
    }
?>
                       <small>Категория: <a href="article.php?id=<?= $category['id_category'] ?>"><?= $article_category['name_category'] ?></a></small>
                      </div>
                      <div class="article__info__preview"><?= mb_substr($article['description_article'], 0, 100, 'utf-8') . '...' ?></div>
                      <div class="delete">
                        <a href="delete_article.php?id=<?=$article['id_article'] ?>" class="form__control" style="text-decoration:none; color:red;">Удалить статью</a>
                      </div>
                    </div>
                  </article>
                    <?php
}
?>
               </div>
              </div>
            </div>
            <div class="block">
              <a href="articles.php?category=1">Все записи</a>
              <h3>Программирование [Новейшее]</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">
                <?php
$articles = mysqli_query($dbconnect, "select * from articles where id_category = 1 order by id_article DESC LIMIT 4");
?>
               <?php
while ($article = mysqli_fetch_assoc($articles)) {
?>
                   <article class="article">
                    <div class="article__image" style="background-image: url(<?= $article['img_article'] ?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?= $article['id_article'] ?>"><?= $article['title_article'] ?></a>
                      <div class="article__info__meta">
                        <?php
    $article_category;
    foreach ($categories as $category) {
      if ($category['id_category'] ==$article['id_category']) {
         
        $article_category = $category;
        
        break;
        }
       
    }
?>
                       <small>Категория: <a href="article.php?id=<?= $category['id_category'] ?>"><?= $article_category['name_category'] ?></a></small>
                      </div>
                      <div class="article__info__preview"><?= substr($article['description_article'], 0, 100) . '...' ?></div>
                      <div class="delete">
                        <a href="delete.php?id=<?=$article['id_article'] ?>" class="form__control" style="text-decoration:none; color:red;">Удалить статью</a>
                      </div>
                    </div>
                  </article>
                    <?php
}
?>
               </div>
              </div>
            </div>
            <div class="block" id="comment-add-form">
              <h3>Добавить статью</h3>
              <div class="block__content">
                <form class="form" method='POST' enctype='multipart/form-data'>
                <?php
if (isset($_POST['create_post'])) {
    $upload_image = $_FILES['img']['name'];
    $folder       = 'images/' . $upload_image;
    move_uploaded_file($_FILES['img']['tmp_name'], $folder);
    mysqli_query($dbconnect, "INSERT INTO articles (`img_article`, `title_article`, `description_article`, `id_category`) VALUES ('" . $folder . "','" . $_POST['title'] . "','" . $_POST['description'] . "','" . $_POST['category'] . "')");
}
?>
                 <div class="form__group">
                    <div class="row">
                      <div class="col-md-6">
                        <input type="text" class="form__control" required="" name="title" placeholder="Заголовок">
                      </div>
                    </div>
                  </div>
                  <div class="form__group">
                    <select name="category" id="" class="form__control">
                    <?php
$categories = mysqli_query($dbconnect, "select * from category");
while ($category = mysqli_fetch_assoc($categories)) {
?>
                       <option name = "category" value="<?= $category['id_category'] ?>"><?= $category['name_category'] ?></option>
            <?php
}
?>
                   </select>
                  </div>
                  <div class="form__group">
                  <input type="file" class="form__control" required="" name="img" >
                  </div>
                  <div class="form__group">
                    <textarea  required="" class="form__control" placeholder="Описание статьи" name="description"></textarea>
                  </div>
                  <div class="form__group">
                    <input type="submit" class="form__control" name="create_post" value="Добавить статью">
                  </div>
                </form>
              </div>
            </div>
          </section>
          <?php
include('sidebar.php');
?>
       </div>
      </div>
    </div>
<?php
include('footer.php');
?>
 </div>
</body>
</html>
<!-- david - i don't care -->