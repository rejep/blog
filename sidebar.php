<section class="content__right col-md-4">
            <div class="block">
              <h3>Мы_знаем</h3>
              <div class="block__content">
                <script type="text/javascript" src="//ra.revolvermaps.com/0/0/6.js?i=02op3nb0crr&amp;m=7&amp;s=320&amp;c=e63100&amp;cr1=ffffff&amp;f=arial&amp;l=0&amp;bv=90&amp;lx=-420&amp;ly=420&amp;hi=20&amp;he=7&amp;hc=a8ddff&amp;rs=80" async="async"></script>
              </div>
            </div>
            <div class="block">
              <h3>Топ читаемых статей</h3>
              <div class="block__content">
                <div class="articles articles__vertical">
                <?php
                $articles = mysqli_query($dbconnect, "select * from articles  order by view DESC LIMIT 5");
                
                ?>
                <?php
                  while($article=mysqli_fetch_assoc($articles))
                  {
                    ?>
                    <article class="article">
                    <div class="article__image" style="background-image: url(<?=$article['img_article']?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?=$article['id_article']?>"><?=$article['title_article']?></a>
                      
                      <div class="article__info__meta">
                        <?php
             $article_category ;
                        foreach($categories as $category)
                        { 
                          if ($category['id_category']=$article['id_category']){
                            $article_category = $category;
                                      break;
        
                          }
                          
                        }
                        ?>
                        <small>Категория: <a href="article.php?id=<?=$category['id_category']?>"><?=$article_category['name_category']?></a></small>
                      </div>
                      <div class="article__info__preview"><?=mb_substr($article['description_article'],0,70, "UTF-8").'...'?></div><br>
                
                    </div>
                  </article>
                    <?php
                  }
                ?>
                </div>
              </div>
            </div>
            <div class="block">
              <h3>Комментарии</h3>
              <div class="block__content">
                <div class="articles articles__vertical">

                  <?php
                  $comments = mysqli_query($dbconnect, "select * from comments order by id_comment DESC LIMIT 5");
                  while($comment = mysqli_fetch_assoc($comments))
                  {
                    ?>
                     
                  <article class="article">
                    <div class="article__image" style="background-image: url(/media/images/post-image.jpg);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?=$comment['id_article']?>"><?=$comment['nickname_author']?></a>
                      <div class="article__info__meta">
               
                      </div>
                      <div class="article__info__preview"><?=substr($comment['text_comment'],0,100)?></div>
                    </div>
                  </article>
                  <?php
                  }
                  ?>
               
                </div>
              </div>
            </div>
          </section>