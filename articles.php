<?php
include('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Блог IT_Минималиста!</title>

  <!-- Bootstrap Grid -->
  <link rel="stylesheet" type="text/css" href="/media/assets/bootstrap-grid-only/css/grid12.css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

  <!-- Custom -->
  <link rel="stylesheet" type="text/css" href="/media/css/style.css">
</head>
<body>

  <div id="wrapper">
<?php 
include('header.php');

?>

    <div id="content">
      <div class="container">
        <div class="row">
          <section class="content__left col-md-8">
            <div class="block">
              <a href="#">Все записи</a>
              <h3>Новейшее_в_блоге</h3>
              <div class="block__content">
                <div class="articles articles__horizontal">

                <?php
                $articles = mysqli_query($dbconnect, "select * from articles where id_category = ".$_GET['category']);
                
                ?>
                <?php
                  while($article=mysqli_fetch_assoc($articles))
                  {
                    ?>
                    <article class="article">
                    <div class="article__image" style="background-image: url(<?=$article['img_article']?>);"></div>
                    <div class="article__info">
                      <a href="article.php?id=<?=$article['id_article']?>"><?=$article['title_article']?></a>
                      
                      <div class="article__info__meta">
                        <?php
                        $article_category;
                        foreach($categories as $category)
                        {
                          if ($category['id_category']=$article['id_category']){
                            $article_category = $category;
                                      break;
        
                          }
                          
                        }
                        ?>
                        <small>Категория: <a href="article.php?id=<?=$category['id_category']?>"><?=$article_category['name_category']?></a></small>
                      </div>
                      <div class="article__info__preview"><?=mb_substr($article['description_article'],0,100, "UTF-8").'...'?></div>
                    </div>
                  </article>
                    <?php
                  }
                
                ?>
                  

          


               

                </div>
              </div>
            </div>


          </section>
          <?php 
          include('sidebar.php');
          
          ?>
        </div>
      </div>
    </div>

<?php
include('footer.php')

?>

  </div>

</body>
</html>